package com.rahasak.runapp

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.rahasak.runapp.protocol.{Activity, Summary}
import com.rahasak.runapp.util.{CsvUtil, DateUtil, HttpUtil, Logger}

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object Main extends App with Logger {

  implicit val system = ActorSystem("runapp")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  CsvUtil.init()
  task1()
  task2()

  def task1(): Unit = {
    // fetch sessions in 15 pages
    // generate activity and save in file
    val f = for {
      a <- Future.sequence((1 to 15).toList.map(i => HttpUtil.fetchSessions(i))).map { r =>
        r.flatten.map { s =>
          Activity(s.id, s.sportTyp,
            DateUtil.toStr(s.startTime,
              DateUtil.DATE_FORMAT),
            DateUtil.toStr(s.startTime,
              DateUtil.DATE_TIME_FORMAT),
            //DateUtil.timeDiffMins(s.startTime, s.endTime).getOrElse(0),
            DateUtil.toMins(s.duration),
            s.distance.toDouble / 1000
          )
        }.filter(_.sportType == 1)
      }
      o <- CsvUtil.saveActivities(a)
    } yield o

    val status = Await.result(f, 1.minute)
    logger.info(s"task1 finished with status - $status")
  }

  def task2(): Unit = {
    // fetch sessions in 15 pages
    // generate summary and save in file
    val f = for {
      s <- Future.sequence((1 to 15).toList.map(i => HttpUtil.fetchSessions(i))).map { r =>
        r.flatten.map { s =>
          Activity(s.id, s.sportTyp,
            DateUtil.toStr(s.startTime,
              DateUtil.DATE_FORMAT),
            DateUtil.toStr(s.startTime,
              DateUtil.DATE_TIME_FORMAT),
            //DateUtil.timeDiffHours(s.startTime, s.endTime).getOrElse(0),
            DateUtil.toHours(s.duration),
            s.distance.toDouble / 1000
          )
        }.groupBy(a => DateUtil.getMonth(a.startTime))
          .map { kv =>
            Summary(
              DateUtil.getYear(kv._2.head.startTime),
              DateUtil.getMonth(kv._2.head.startTime),
              kv._2.map(_.duration).sum,
              kv._2.map(_.distance).sum
            )
          }
      }
      o <- CsvUtil.saveSummaries(s.toList)
    } yield o

    val status = Await.result(f, 1.minute)
    logger.info(s"task2 finished with status - $status")
  }

}
