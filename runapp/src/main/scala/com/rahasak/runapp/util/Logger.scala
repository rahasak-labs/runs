package com.rahasak.runapp.util

import java.io.{PrintWriter, StringWriter}

import org.slf4j

trait Logger {

  val logger = slf4j.LoggerFactory.getLogger(this.getClass)

  def logError(throwable: Throwable): Unit = {
    val writer = new StringWriter
    throwable.printStackTrace(new PrintWriter(writer))
    logger.error(writer.toString)
  }
}

