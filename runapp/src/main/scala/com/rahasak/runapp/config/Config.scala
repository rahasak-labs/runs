package com.rahasak.runapp.config

import com.typesafe.config.ConfigFactory

import scala.util.Try

/**
  * Load configurations define in application.conf from here
  *
  * @author eranga herath(erangaeb@gmail.com)
  */
trait Config {

  // config object
  val conf = ConfigFactory.load()
  lazy val runApi = Try(conf.getString("api.run")).getOrElse("http://intense-bastion-3210.herokuapp.com")
  lazy val outDir = Try(conf.getString("csv.out-dir")).getOrElse(".out")

}
