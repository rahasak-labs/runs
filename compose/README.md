# Compose

`docker-compose` deployment for `runs`. 


## 1. Runapp

`runapp` is the service corresponds with coding exercise. The service written
with `scala` language and `akka-streams`. The source code of the service can be 
found from [here](https://gitlab.com/rahasak-labs/runs/tree/master/runapp). The `runapp` can be deployed 
and test with `docker`. The configurations of the services load with env variables(`12factorapps`). 
Before deploying the service change the `.env` files `CSV_OUT_DIR` into local machine 
path(it will be the. the location .csv outputs of `task1` and `taks2`).

```
CSV_OUT_DIR=/private/var/services/runs/runapp
```

Deploy the `runapp` with `docker-compose`. The output .csv files can be found 
in `CSV_OUT_DIR` directory.


```
docker-compose up -d runapp
```

Service logs can be view via docker

```
docker logs runapp
```


## 2. Qops 

`qops` is the sql exercise. This exercise is based on `mysql` data storage. The 
source codes of this exercise can be found from [here](https://gitlab.com/rahasak-labs/runs/tree/master/qops). 
First need to run and configure `mysql`. Following is the way to run mysql with 
`docker-compose`.

```
# run mysql
docker-compose up -d mysql

# connect to mysql docker container
docker exec -it mysql bash

# connect to mysql
# root password = root
mysql -u root -p -h dev.localhost

# disable only_full_group_by feature
set global sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
set session sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';

# create database
create database qops;
use qops;
```

### 2.1 Query1

Bootstrap the data corresponding for `query1.sql`(in [here](https://gitlab.com/rahasak-labs/runs/blob/master/qops/query1.sql)). 
Then run following query to view the output.

```
# query
SELECT contacts.first_name, 
       contacts.last_name, 
       emails.email, 
       emails.email_type 
FROM   contacts, 
       emails 
WHERE  contacts.id = emails.contact_id 
       AND email_type <> 'private' 
       AND emails.id IN (SELECT Max(id) 
                         FROM   emails 
                         GROUP  BY contact_id);

# output
+------------+-----------+----------------------------+------------+
| first_name | last_name | email                      | email_type |
+------------+-----------+----------------------------+------------+
| Jessica    | Johnson   | jessica.johnson@adidas.com | office     |
| Michael    | Jones     | michael.jones@hotmail.com  | office     |
| Ralf       | Schmid    | ralf.schmid@zalando.com    | office     |
+------------+-----------+----------------------------+------------+
``` 

Following is the query to load the query result to .csv file.

```
SELECT contacts.first_name, 
       contacts.last_name, 
       emails.email, 
       emails.email_type 
FROM   contacts, 
       emails 
WHERE  contacts.id = emails.contact_id 
       AND email_type <> 'private' 
       AND emails.id IN (SELECT Max(id) 
                         FROM   emails 
                         GROUP  BY contact_id)
INTO OUTFILE '/var/lib/mysql-files/query1.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n';
```

The query result will be loaded into `/var/lib/mysql-files/query1.csv` inside the `mysql` 
docker container. You can copy the file to local machine via `docker cp`.

```
docker cp mysql:/var/lib/mysql-files/query1.csv .
```


### 2.2 Query2

Bootstrap the data corresponding for `query2.sql`(in [here](https://gitlab.com/rahasak-labs/runs/blob/master/qops/query2.sql)). 
Then run following query to view the output.

```
# query
SELECT a.salesperson_id, 
       a.id, 
       a.date 
FROM   transactions AS a 
WHERE  (SELECT Count(*) 
        FROM   transactions AS b 
        WHERE  b.salesperson_id = a.salesperson_id 
               AND b.date >= a.date) <= 2 
ORDER  BY a.salesperson_id ASC, 
          a.date ASC;

# output
+----------------+------+------------+
| salesperson_id | id   | date       |
+----------------+------+------------+
|              1 |    3 | 2015-01-03 |
|              1 |    5 | 2015-01-04 |
|              2 |    6 | 2015-01-04 |
|              2 |    7 | 2015-01-05 |
|              3 |   10 | 2015-01-07 |
|              3 |   11 | 2015-01-08 |
+----------------+------+------------+
``` 

Following is the query to load the query result to .csv file.

```
SELECT a.salesperson_id, 
       a.id, 
       a.date 
FROM   transactions AS a 
WHERE  (SELECT Count(*) 
        FROM   transactions AS b 
        WHERE  b.salesperson_id = a.salesperson_id 
               AND b.date >= a.date) <= 2 
ORDER  BY a.salesperson_id ASC, 
          a.date ASC
INTO OUTFILE '/var/lib/mysql-files/query2.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n';
```

The query result will be loaded into `/var/lib/mysql-files/query2.csv` inside the `mysql` 
docker container. You can copy the file to local machine via `docker cp`.

```
docker cp mysql:/var/lib/mysql-files/query2.csv .
```
