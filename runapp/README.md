# Runapp

`runs` coding exercise

## Build

Build with `docker`

```
sbt assembly
docker build erangaeb/runapp:0.1 .
```

## Run

Run via `docker-compose` [here](https://gitlab.com/rahasak-labs/runs/tree/master/compose)

