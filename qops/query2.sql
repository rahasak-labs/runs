/* SQL Query 2: Two Latest Transactions by Salesperson */

/*Supporting tables if needed */
CREATE TABLE transactions( id int, date date, total int, salesperson_id int );
INSERT INTO transactions(id, date, total, salesperson_id) Values('1', '2015-01-01', '3234', '1');
INSERT INTO transactions(id, date, total, salesperson_id) Values('1', '2015-01-01', '3234', '1');
INSERT INTO transactions(id, date, total, salesperson_id) Values('3', '2015-01-03', '2500', '1');
INSERT INTO transactions(id, date, total, salesperson_id) Values('4', '2015-01-03', '1100', '2');
INSERT INTO transactions(id, date, total, salesperson_id) Values('5', '2015-01-04', '3100', '1');
INSERT INTO transactions(id, date, total, salesperson_id) Values('6', '2015-01-04', '1150', '2');
INSERT INTO transactions(id, date, total, salesperson_id) Values('7', '2015-01-05', '2100', '2');
INSERT INTO transactions(id, date, total, salesperson_id) Values('8', '2015-01-05', '1100', '3');
INSERT INTO transactions(id, date, total, salesperson_id) Values('9', '2015-01-06', '5100', '3');
INSERT INTO transactions(id, date, total, salesperson_id) Values('10', '2015-01-07', '3100', '3');
INSERT INTO transactions(id, date, total, salesperson_id) Values('11', '2015-01-08', '2100', '3');


/* query */
SELECT a.salesperson_id, 
       a.id, 
       a.date 
FROM   transactions AS a 
WHERE  (SELECT Count(*) 
        FROM   transactions AS b 
        WHERE  b.salesperson_id = a.salesperson_id 
               AND b.date >= a.date) <= 2 
ORDER  BY a.salesperson_id ASC, 
          a.date ASC;


/* query result to csv */
SELECT a.salesperson_id, 
       a.id, 
       a.date 
FROM   transactions AS a 
WHERE  (SELECT Count(*) 
        FROM   transactions AS b 
        WHERE  b.salesperson_id = a.salesperson_id 
               AND b.date >= a.date) <= 2 
ORDER  BY a.salesperson_id ASC, 
          a.date ASC
INTO OUTFILE '/var/lib/mysql-files/query2.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n';
