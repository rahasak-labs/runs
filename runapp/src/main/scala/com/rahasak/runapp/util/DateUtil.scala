package com.rahasak.runapp.util

import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit
import java.util.{Date, TimeZone}

object DateUtil {

  val TIME_ZONE = TimeZone.getTimeZone("UTC")
  val TIMESTAMP_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
  val DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss"
  val DATE_FORMAT = "yyyy-MM-dd"

  def toDate(date: Option[String]): Option[Date] = {
    val sdf = new SimpleDateFormat(TIMESTAMP_FORMAT)
    sdf.setTimeZone(TIME_ZONE)
    date.map(d => sdf.parse(d))
  }

  def toStr(date: Option[Date], format: String): Option[String] = {
    date match {
      case Some(d) =>
        val sdf = new SimpleDateFormat(format)
        sdf.setTimeZone(TIME_ZONE)
        Option(sdf.format(d))
      case None =>
        None
    }
  }

  def timeDiffMins(start: Option[Date], end: Option[Date]): Option[Long] = {
    for (e <- end; s <- start) yield TimeUnit.MILLISECONDS.toMinutes(e.getTime - s.getTime)
  }

  def timeDiffHours(start: Option[Date], end: Option[Date]): Option[Long] = {
    for (e <- end; s <- start) yield TimeUnit.MILLISECONDS.toHours(e.getTime - s.getTime)
  }

  def toMins(duration: Long): Long = {
    TimeUnit.MILLISECONDS.toMinutes(duration)
  }

  def toHours(duration: Long) = {
    TimeUnit.MILLISECONDS.toHours(duration)
  }

  def getMonth(date: Option[String]): String = {
    date.map(_.split("-")(1)).getOrElse("")
  }

  def getYear(date: Option[String]): String = {
    date.map(_.split("-")(0)).getOrElse("")
  }

}
