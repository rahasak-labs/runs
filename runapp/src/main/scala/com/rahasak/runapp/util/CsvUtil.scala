package com.rahasak.runapp.util

import java.io.{File, FileWriter}

import com.rahasak.runapp.config.Config
import com.rahasak.runapp.protocol.{Activity, Summary}

import scala.concurrent.{ExecutionContext, Future}

object CsvUtil extends Config with Logger {

  def init(): Unit = {
    // first create .out directory
    val dir: File = new File(".out")
    if (!dir.exists) {
      dir.mkdir
    }
  }

  def saveActivities(activities: List[Activity])(implicit ex: ExecutionContext): Future[String] = {
    Future {
      val fw = new FileWriter(s"$outDir/activity.csv")
      activities.foreach { p =>
        fw.write(p.toCSV())
        fw.write("\r\n")
      }
      fw.close()

      logger.info("wrote activities to csv")
      "Done"
    }.recover {
      case e =>
        logError(e)
        e.getMessage
    }
  }

  def saveSummaries(summaries: List[Summary])(implicit ex: ExecutionContext): Future[String] = {
    Future {
      val fw = new FileWriter(s"$outDir/summary.csv", false)
      summaries.foreach { p =>
        fw.write(p.toCSV())
        fw.write("\r\n")
      }
      fw.close()

      logger.info("wrote summaries to csv")
      "Done"
    }.recover {
      case e =>
        logError(e)
        e.getMessage
    }
  }

}
