package com.rahasak.runapp.util

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import com.rahasak.runapp.config.Config
import com.rahasak.runapp.protocol.{Response, RunSession}
import spray.json._

import scala.concurrent.Future

object HttpUtil extends Config with Logger {

  def fetchSessions(page: Int)(implicit system: ActorSystem, mat: ActorMaterializer): Future[List[RunSession]] = {
    implicit val ex = system.dispatcher
    val url = s"$runApi/?$page"

    Http(system).singleRequest(
      HttpRequest(
        HttpMethods.GET,
        url)
    ).flatMap { response =>
      Unmarshal(response.entity).to[String]
    }.map { responseStr =>
      logger.info(s"got response $responseStr")
      import com.rahasak.runapp.protocol.ResponseProtocol._
      responseStr.parseJson.convertTo[Response]
    }.map { response =>
      logger.info(s"success get response $response")
      response.runSessions
    }.recover {
      case e =>
        logError(e)
        e.printStackTrace()
        List()
    }
  }

}
