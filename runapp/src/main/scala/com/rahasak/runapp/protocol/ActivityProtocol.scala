package com.rahasak.runapp.protocol

case class Activity(id: Long, sportType: Int, startDate: Option[String], startTime: Option[String], duration: Long, distance: Double, pace: Double)

object Activity {
  def apply(id: Long, sportType: Int, startDate: Option[String], startTime: Option[String], duration: Long, distance: Double): Activity = {
    new Activity(id, sportType, startDate, startTime, duration, distance, if(duration == 0 || distance == 0) 0 else distance.toDouble/duration)
  }

  implicit class CSVWrapper(val prod: Activity) extends AnyVal {
    def toCSV() = prod.productIterator.map {
      case Some(value) => value
      case None => ""
      case rest => rest
    }.mkString(",")
  }
}


