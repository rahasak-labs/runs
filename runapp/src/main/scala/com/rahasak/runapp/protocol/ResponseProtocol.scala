package com.rahasak.runapp.protocol

import spray.json.{DefaultJsonProtocol, JsValue, RootJsonFormat}

case class Response(meta: Meta, runSessions: List[RunSession])

object ResponseProtocol extends DefaultJsonProtocol {

  import PaginationProtocol._
  import RunSessionProtocol._

  implicit val metaFormat = jsonFormat1(Meta)

  implicit object ResponseFormat extends RootJsonFormat[Response] {
    def write(obj: Response) = ???

    def read(json: JsValue): Response = {
      val fields = json.asJsObject.fields
      Response(
        fields("meta").convertTo[Meta],
        fields("run_sessions").convertTo[List[RunSession]]
      )
    }
  }

}

