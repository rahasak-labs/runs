package com.rahasak.runapp.protocol

import java.util.Date

import com.rahasak.runapp.util.DateUtil
import spray.json.{DefaultJsonProtocol, JsValue, RootJsonFormat}

case class RunSession(id: Long, startTime: Option[Date], endTime: Option[Date], duration: Long, distance: Long, encodedTrace: Option[String], sportTyp: Int)

object RunSessionProtocol extends DefaultJsonProtocol {

  implicit object RunSessionFormat extends RootJsonFormat[RunSession] {
    def write(obj: RunSession) = ???

    def read(json: JsValue): RunSession = {
      val fields = json.asJsObject.fields
      RunSession(
        fields("id").convertTo[Long],
        DateUtil.toDate(fields("start_time").convertTo[Option[String]]),
        DateUtil.toDate(fields("end_time").convertTo[Option[String]]),
        fields("duration").convertTo[Long],
        fields("distance").convertTo[Long],
        fields("encoded_trace").convertTo[Option[String]],
        fields("sport_type_id").convertTo[Int]
      )
    }
  }

}

