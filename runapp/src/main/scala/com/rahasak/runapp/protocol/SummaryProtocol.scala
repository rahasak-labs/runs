package com.rahasak.runapp.protocol

case class Summary(year: String, month: String, duration: Double, distance: Double, pace: Double)

object Summary {
  def apply(year: String, month: String, duration: Double, distance: Double): Summary = {
    new Summary(year, month, duration, distance, duration / distance)
  }

  implicit class CSVWrapper(val prod: Summary) extends AnyVal {
    def toCSV() = prod.productIterator.map {
      case Some(value) => value
      case None => ""
      case rest => rest
    }.mkString(",")
  }

}


