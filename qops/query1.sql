/* SQL Query 1: Several Office Email Addresses - One Person */

/*Supporting tables if needed */
CREATE TABLE contacts( id int, first_name varchar(200), last_name varchar(200) );
INSERT INTO contacts (id, first_name, last_name) Values('1','Peter','Parker');
INSERT INTO contacts (id, first_name, last_name) Values('2','Jessica','Johnson');
INSERT INTO contacts (id, first_name, last_name) Values('3','Michael','Jones');
INSERT INTO contacts (id, first_name, last_name) Values('4','Ralf','Schmid');
INSERT INTO contacts (id, first_name, last_name) Values('5','Leopold','Jackson');

CREATE TABLE emails ( id int, contact_id int, email varchar(200), email_type varchar(15) );
INSERT INTO emails (id, contact_id, email, email_type) Values('1', '1', 'peter.parker@runtastic.com', 'office');
INSERT INTO emails (id, contact_id, email, email_type) Values('2', '1', 'peter.parker@gmail.com', 'private');
INSERT INTO emails (id, contact_id, email, email_type) Values('3', '2', 'jessica.johnson@runtastic.com', 'office');
INSERT INTO emails (id, contact_id, email, email_type) Values('4', '2', 'jessica.johnson@adidas.com', 'office');
INSERT INTO emails (id, contact_id, email, email_type) Values('5', '3', 'michael.jones@hotmail.com', 'office');
INSERT INTO emails (id, contact_id, email, email_type) Values('6', '4', 'ralf.schmid@amazon.com', 'office');
INSERT INTO emails (id, contact_id, email, email_type) Values('7', '4', 'ralf.schmid@rocket.com', 'office');
INSERT INTO emails (id, contact_id, email, email_type) Values('8', '4', 'ralf.schmid@zalando.com', 'office');
INSERT INTO emails (id, contact_id, email, email_type) Values('9', '5', 'l.jackson@googlemail.com', 'private');


/* query */
SELECT contacts.first_name, 
       contacts.last_name, 
       emails.email, 
       emails.email_type 
FROM   contacts, 
       emails 
WHERE  contacts.id = emails.contact_id 
       AND email_type <> 'private' 
       AND emails.id IN (SELECT Max(id) 
                         FROM   emails 
                         GROUP  BY contact_id);


/* query result to csv */
SELECT contacts.first_name, 
       contacts.last_name, 
       emails.email, 
       emails.email_type 
FROM   contacts, 
       emails 
WHERE  contacts.id = emails.contact_id 
       AND email_type <> 'private' 
       AND emails.id IN (SELECT Max(id) 
                         FROM   emails 
                         GROUP  BY contact_id)
INTO OUTFILE '/var/lib/mysql-files/query1.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n';
