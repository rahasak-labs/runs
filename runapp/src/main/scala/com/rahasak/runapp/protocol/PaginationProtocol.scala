package com.rahasak.runapp.protocol

import spray.json.{DefaultJsonProtocol, JsValue, RootJsonFormat}

case class Meta(pagination: Pagination)

case class Pagination(perPage: Int, availablePages: Int, total: Int, page: Int, sortBy: String, order: String)

object PaginationProtocol extends DefaultJsonProtocol {

  implicit object PaginationFormat extends RootJsonFormat[Pagination] {
    def write(obj: Pagination) = ???

    def read(json: JsValue): Pagination = {
      val fields = json.asJsObject.fields
      Pagination(
        fields("per_page").convertTo[Int],
        fields("available_pages").convertTo[Int],
        fields("total").convertTo[Int],
        fields("page").convertTo[Int],
        fields("sort_by").convertTo[String],
        fields("order").convertTo[String]
      )
    }
  }

}

